<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/25/14
 * Time: 6:40 PM
 */
return [
    'krona' => [
        'types' => [
            'converters' => [
                'id' => \Krona\MongoODM\Common\IdConverter::class,
                'date' => \Krona\Common\Common\Converter\DatetimeConverter::class,
                'datetime' => \Krona\Common\Common\Converter\DatetimeConverter::class,
                'mongoDate' => \Krona\MongoODM\Common\DateTimeConverter::class,
                'association' => \Krona\Common\Common\Converter\AssociationConverter::class,
                'array' => \Krona\Common\Common\Converter\ArrayConverter::class,
                'integer' => \Krona\Common\Common\Converter\IntegerConverter::class,
            ],
        ],
        'mongo' => [
            'configuration' => [
                'orm_default' => [
                    'cache_dir' => 'data/KronaODM',
                    'debug' => true,
                ],
            ],
            'connection' => [
                'orm_default' => [
                    'params' => [
//                        'user' => '',
//                        'password' => '',
                        'host' => 'localhost',
                        'port' => '27017',
                        'dbname' => '',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'invokables' => [
            'krona.mongoodm.registry' => \Krona\MongoODM\Service\ManagerFactory::class,
            'krona.common.inputfilter.generator' => \Krona\Common\Service\InputFilterGenerator::class
        ]
    ],
];