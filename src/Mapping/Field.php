<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:52 PM
 */

namespace Krona\MongoODM\Mapping;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Field
 * @package Krona\MongoODM\Mapping
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Field
{
    public $name;

    public $type;
} 