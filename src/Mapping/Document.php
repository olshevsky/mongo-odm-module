<?php
/**
 * User: krona
 * Date: 9/29/14
 * Time: 6:53 PM
 */

namespace Krona\MongoODM\Mapping;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Document
 * @package Krona\MongoODM\Mapping
 * @Annotation
 * @Target({"CLASS"})
 */
class Document
{
    public $repositoryClass;
} 