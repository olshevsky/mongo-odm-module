<?php
/**
 * User: krona
 * Date: 9/30/14
 * Time: 6:48 PM
 */

namespace Krona\MongoODM\Common;


use Krona\Common\Common\Converter\ConverterInterface;
use Krona\MongoODM\Exception\RuntimeException;

class IdConverter implements ConverterInterface
{
    /**
     * Convert to PHP type
     * @param \MongoId $value
     * @return \MongoId
     */
    public function convert($value)
    {
        if (is_array($value)) {
            if (isset($value['$id'])) {
                return $value['$id'];
            } else {
                throw new RuntimeException(
                    'Unknown type of id'
                );
            }
        } else {
            return (string)$value;
        }
    }

    /**
     * Convert to SQL type
     * @param      $value
     * @param bool $asColumn
     * @return mixed
     */
    public function revert($value, $asColumn = true)
    {
        if ($asColumn) {
            if (is_null($value)) {
                return new \MongoId();
            } else {
                return new \MongoId($value);
            }
        } else {
            return $value;
        }
    }
}